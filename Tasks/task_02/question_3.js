// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

var assert = require('assert');
describe('Question 3', function() {
    it('test', function() {
        var numbers = [32, 12, 83, 13, 4, 28];
        for (i = 0; i < numbers.length; i++){
            numbers[i] = numbers[i] * 3;
        }
        assert.deepEqual(numbers, [96, 36, 249, 39, 12, 84]);
    });
});
