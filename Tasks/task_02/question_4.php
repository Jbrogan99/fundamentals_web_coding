<?php
// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

class Question4Test extends PHPUnit\Framework\TestCase {
    public function test() {
        $numbers = array(32, 12, 83, 13, 4, 28);
        for ($idx = 0; $idx < count($numbers); $idx++){
        if ($numbers[$idx] % 2 == 0) {
        $numbers[$idx] = $numbers[$idx] / 2;    
        } else {
        $numbers[$idx] = $numbers[$idx] * 2;
        } }
       
        $this->assertEquals([16, 6, 166, 26, 2, 14], $numbers);
    }
}

