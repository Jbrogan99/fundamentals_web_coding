// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

var assert = require('assert');
describe('Question 5', function() {
    it('test', function() {
        var number = 3;
        var count = 0;
                while(number * 2){
                number += number;
                count += 1;
                if(number > 21297){
                        break;
                } 
                }
        assert.deepEqual(count, 13);
    });
});
