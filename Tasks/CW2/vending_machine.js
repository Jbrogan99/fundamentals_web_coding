readline = require("readline-sync"); //Libary imported that allows user input into terminal

var vending_selection = [
  // Array of objects that hold snack name, price and other options

  { name: "Mars", price: 1.5 },
  { name: "Dairy Milk", price: 1.2 },
  {
    name: "Caramac",
    price: 1.0,
  },
  {
    name: "Malteasers",
    price: 0.8,
  },
  {
    name: "Aero",
    price: 1.1,
  },
  {
    name: "Coke Original",
    price: 1.2,
  },
  {
    name: "Diet Coke",
    price: 0.8,
  },
  {
    name: "Lucozade",
    price: 1.5,
  },
  {
    name: "Sprite",
    price: 0.8,
  },
  {
    name: "Refund Creidt",
  },
  {
    name: "Add Money",
  },
  {
    name: "Exit",
  },
];

var balanceObj = {
  // Balance object created to allow pass by reference so balance number will update
  balance: 0, // Stores balance value
};

function main_menu(cash) {
  console.log("Current balance: £" + cash + " \n"); // Displays balance to user
  vending_selection.forEach((food, index) => {
    //ForEach Loop to iterate over each object and index number
    if (index == 9 || index == 10 || index == 11) {
      console.log(index + ":" + " " + food.name + "\n"); // Display only name for non food options
    } else {
      console.log(
        index + ":" + " " + food.name + " " + "£" + food.price + "\n"
      ); //Displays name and price for food items
    }
  });
}

main_menu(balanceObj.balance); // Call the main menu upon load
user_selection(); // Call the user selection upon load

function user_selection(choice) {
  //onlyNumbers
  var choice = readline.question("Enter Product Number:"); // Stores user input
  var choiceNum = parseInt(choice); // converting the returned string to a number
  var onlyNumbers = /\d+/g; // Enbales only numbers to be passed into product number
  switch (choiceNum) { //switch statement enabling user selection 
    case 0: //cases 1-8 allows user to pick product  
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
      user_purchase(
        balanceObj,
        vending_selection[choiceNum].price, // Extracting data from objects and placing them into the function
        vending_selection[choiceNum].name
      );
      break;// break out of the cases 1-8 
    case 9:
      refundCredit(balanceObj); // allows user to refund credit 
      break;
    case 10:
      addcredit();// allows user to add credit 
      break;
    case 11:
      exit();// allows user to exit
      break;
  }
  if (choice >= 12 || choice != onlyNumbers) { // ensures only numbers are inputed by the user
    incorrectOption();
  }
  
}

function incorrectOption() {
  //Function alerting user to enter a valid number
  console.log("Sorry you need to pick a valid number");
  main_menu(balanceObj.balance.toFixed(2));
  user_selection();
}

function exit() {
  // Allows user to exit the vending machine
  console.log("Thank you, you have now left the virtual vending machine "); //logs message to user 
  process.exit();
}

function addcredit() {
  var addCredit = readline.question(" Add Credit: £ ");
  if (addCredit != NaN && addCredit > 0) {
    // if statment ensures user enters valid number
    var IntCredit = parseFloat(addCredit); // Changes the "string number" to floating point number
    balanceObj.balance += IntCredit; //Adding the balance to the users input
    main_menu(balanceObj.balance); //Calling the main menu with the updated balance
    user_selection(); // Call user selection function
  } else {
    console.log("Invalid amount please enter a valid number");
    main_menu(balanceObj.balance); //Calling the main menu with the updated balance
    user_selection(); // Call user selection function
  }
}

function refundCredit(cashObj) {
  if (cashObj.balance > 0) {
    //Ensures the user has credit in system to be refunded
    var check = readline.question(
      "Are you sure you would like to refund credit? y/n \n" //checks if the user meant to press refund option
    );
    if (check == "y") {
      // if user says yes
      cashObj.balance -= cashObj.balance; // Deductes balance from the balance
      console.log("Your credit has been refunded");
      main_menu(cashObj.balance); //Calling the main menu with the updated balance
      user_selection(); // Call user selection function
    }
    if (check == "n") {
      console.log("Your money has not been refunded");
      main_menu(cashObj.balance); //Calling the main menu with the updated balance
      user_selection(); // Call user selection function
    }
  } else {
    console.log("You need to add money before you can get a refund!"); // Provides message to the user so they know money needs to be added before refund
    main_menu(cashObj.balance); //Calling the main menu with the updated balance
    user_selection(); // Call user selection function
  }
}

function user_purchase(cashObj, price, snack) {
  if (cashObj.balance >= price) {
    // Ensures balance is greater or equal to price
    var result = (cashObj.balance -= price);
    result = result.toFixed(2); // Gives a result to two decimal place
    console.log(
      `Thank you for your purchase, you have chosen ${snack}. Your remaining balance is £${result} \n`
    ); // Alert user of choice and balance
    main_menu(result); //Calling the main menu with the updated balance
    user_selection(); // Call user selection function
  } else {
    var balanceTwoDecimalPlace = cashObj.balance;
    balanceTwoDecimalPlace = balanceTwoDecimalPlace.toFixed(2);
    console.log(
      `Insifficent Funds. Your current balance is £${balanceTwoDecimalPlace}. Enter option 10 to add more money! \n`
    ); // Alert user that they don't have enough money
    main_menu(cashObj.balance.toFixed(2)); //Calling the main menu with the updated balance
    user_selection(); // Call user selection function
  }
}
