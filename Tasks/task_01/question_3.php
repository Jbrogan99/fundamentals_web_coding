<?php
// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

class Question3Test extends PHPUnit\Framework\TestCase {
    public function test() {
        $name = 'Fred';
        $welcome = 'Welcome to University, ' . $name;
        $this->assertEquals('Welcome to University, Fred', $welcome);
    }
}

