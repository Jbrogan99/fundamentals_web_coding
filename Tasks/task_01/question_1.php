<?php
// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

class Question1Test extends PHPUnit\Framework\TestCase {
    public function test() {
        $a = 4;
        $b = 2;
        $c = $a + $b;
        $this->assertEquals(4, $a);
        $this->assertEquals(2, $b);
        $this->assertEquals(6, $c);
    }
}

