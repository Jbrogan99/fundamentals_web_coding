// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

var assert = require('assert');
describe('Question 4', function() {
    it('test', function() {
        var name = 'Fred';
        var welcome = "Welcome to University, " + name;
        assert.equal(welcome, 'Welcome to University, Fred');
    });
});
