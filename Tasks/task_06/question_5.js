// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

// StartStudentCode
function interest3(amount, rate, years){
  if (years > 0) {
      var ratePercentage = rate / 100 + 1;
      return ratePercentage * interest3(amount, rate, years-1) 
  } 
  else
  {
      return 1;
  }
}


function interest(amount, years, rate){
  return amount*interest3(amount, rate, years);
}


// EndStudentCode

var assert = require('assert');
describe('Question 5', function() {
    it('test', function() {
        assert.equal(interest(100, 0, 4), 100);
        assert.equal(interest(100, 1, 4), 104);
        assert.equal(interest(100, 5, 4).toFixed(4), 121.6653);
        assert.equal(interest(100, 8, 3).toFixed(4), 126.677);
    });
});
