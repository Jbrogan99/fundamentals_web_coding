<?php
// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

// StartStudentCode
class Automobile {
    public function __construct($make, $model, $wheels){
        $this->make = $make;
        $this->model = $model;
        $this->wheels = $wheels;
    }
   
    public function summary(){
        return $this->make . " " . $this->model . " " . "(" . $this->wheels . " " . "wheels" . ")";
    }
}

class Car extends Automobile{
    public function __construct($make, $model){
        parent::__construct($make, $model, $this);
        $this->wheels = 4;
    }
}


class Bike extends Automobile{
    public function __construct($make, $model){
        parent::__construct($make, $model, $this);
        $this->wheels = 2;
    }
}
// EndStudentCode

class Question3Test extends PHPUnit\Framework\TestCase {
    public function test() {
        $bmw = new Car('BMW', 'Z2');
        $this->assertEquals('BMW', $bmw->make);
        $this->assertEquals('Z2', $bmw->model);
        $this->assertEquals(4, $bmw->wheels);
        $this->assertEquals('BMW Z2 (4 wheels)', $bmw->summary());
        $ktm = new Bike('KTM', '1050 Adventure');
        $this->assertEquals('KTM', $ktm->make);
        $this->assertEquals('1050 Adventure', $ktm->model);
        $this->assertEquals(2, $ktm->wheels);
        $this->assertEquals('KTM 1050 Adventure (2 wheels)', $ktm->summary());
    }
}

