<?php
// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

// StartStudentCode
// EndStudentCode

class Question3Test extends PHPUnit\Framework\TestCase {
    public function test() {
    function grade($mark){
     if ($mark >= 70){
        return "First";
    } if ($mark < 70 && $mark >= 60 ){
        return "2:1";
    } if ($mark < 60 && $mark >= 50){
        return "2:2";
    } if ($mark < 50 && $mark >= 40){
        return "Third";
    } if ($mark < 40){
        return "Fail";
    }
}
        $this->assertEquals('First', grade(75));
        $this->assertEquals('First', grade(70));
        $this->assertEquals('2:1', grade(63));
        $this->assertEquals('2:2', grade(56));
        $this->assertEquals('Third', grade(44));
        $this->assertEquals('Fail', grade(21));
    }
}

