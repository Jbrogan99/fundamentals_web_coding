// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

// StartStudentCode
// EndStudentCode

var assert = require('assert');
describe('Question 4', function() {
    it('test', function() {
        function reverse(list){
        return list.reverse();
    }
    var array = [3, 4];
    reverse(array);
    assert.deepEqual(reverse([3, 4]), [4, 3]);
    });
});
